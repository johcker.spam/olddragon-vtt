/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class OldDragonActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["olddragon", "sheet", "actor"],
      template: "systems/olddragon/templates/actor/actor-sheet.html",
      width: 840,
      height: 780,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "principal" }]
    });
  }


  /* -------------------------------------------- */

  /** @override */
  async getData(options) {
        
    let isOwner = this.actor.isOwner;
    const data = {
      owner: isOwner,
      limited: this.actor.limited,
      options: this.options,
      editable: this.isEditable,
      cssClass: isOwner ? "editable" : "locked",
      isCharacter: this.actor.type === "character",
      rollData: this.actor.getRollData.bind(this.actor)
    };

    // The Actor's data

    data.actor = duplicate(this.document);
    data.actor.name = this.document.name;
    data.system = duplicate(this.document.system);
    data.labels = this.document.labels || {}
    data.filters = this._filters
    
    //console.log(actorData);
    // console.log(data.actor);
    //console.log(data.system);

    // Owned Items
    data.items = data.actor.items;
    for ( let i of data.items ) {
      const item = this.actor.items.get(i._id);
      i.labels = item.labels;
    }
    data.items.sort((a, b) => (a.sort || 0) - (b.sort || 0));

    if (data.isCharacter) {
      this._prepareCharacterItems(data);
    }
    
    return data
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareCharacterItems(sheetData) {
    //console.log(sheetData);
    const actorData = sheetData;

    const itens = [];
    const armas_cac = [];
    const armas_dis = [];
    const armaduras = [];
    const aneis = [];
    const escudos = [];
    const outros_equip = [];
    const pocoes = [];
    const pergaminhos = [];
    const propriedades = [];
    const magias = [];
    const talentos = [];
    const dominios = [];

    //fixme
    for (let i of sheetData.items) {
      let item = i.system;
      i.img = i.img || DEFAULT_TOKEN;

      if (i.type === 'item') {
        itens.push(i);
      }

      else if (i.type === 'arma_cac') {
        armas_cac.push(i);
      }

      else if (i.type === 'arma_dis') {
        armas_dis.push(i);
      }

      else if (i.type === 'armadura') {
        armaduras.push(i);
      }

      else if (i.type === 'anel') {
        aneis.push(i);
      }

      else if (i.type === 'escudo') {
        escudos.push(i);
      }

      else if (i.type === 'outro_equip') {
        outros_equip.push(i);
      }

      else if (i.type === 'pocao') {
        pocoes.push(i);
      }

      else if (i.type === 'pergaminho') {
        pergaminhos.push(i);
      }

      else if (i.type === 'propriedade') {
        propriedades.push(i);
      }

      else if (i.type === 'magia') {
        magias.push(i);
      }

      else if (i.type === 'talento') {
        talentos.push(i);
      }

      else if (i.type === 'dominio') {
        dominios.push(i);
      }

    }
    actorData.itens = itens;
    actorData.armas_cac = armas_cac;
    actorData.armas_dis = armas_dis;
    actorData.armaduras = armaduras;
    actorData.aneis = aneis;
    actorData.escudos = escudos;
    actorData.outros_equip = outros_equip;
    actorData.pocoes = pocoes;
    actorData.pergaminhos = pergaminhos;
    actorData.propriedades = propriedades;
    actorData.magias = magias;
    actorData.talentos = talentos;
    actorData.dominios = dominios;

  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    const dragHandler = ev => this._onDragStart(ev)

    // Add Inventory Item      
    html.find('.item-create').click(async (event) => {
      event.preventDefault();
      const header = event.currentTarget;
      let type = header.dataset.type;
      let createdItem;
      // item creation helper func
      let createItem = function (type, name = `Novo(a) ${type.capitalize()}`) {
        const itemData = {
          name: name ? name : `Novo(a) ${type.capitalize()}`,
          type: type,
          //data: duplicate(header.dataset),
          system: duplicate(header.dataset),
        };
        delete itemData.system['type'];
        return itemData;
      };
      const itemData = createItem(type);

      //alteração para criação de item
      createdItem = await Item.create(itemData, {parent: this.actor});
      //createdItem = this.actor.createEmbeddedDocuments('Item', [itemData]);
      this.actor.items.get(createdItem.id).sheet.render(true);

    });

    //Toggle Equipment
    html.find('.item-toggle').click(this._onToggleEquipped.bind(this));

    //Toggle Equipment
    html.find('.item-togglemagic').click(this._onToggleMemorizada.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Item
    html.find('.item-delete').click(async (ev) => {
      const li = $(ev.currentTarget).parents('.item');

      let ownedItem = this.actor.items.get(li.data('itemId'));
      //console.log(ownedItem);
      const template = `
    <form>
      <div>
        <center>Deletar 
          <strong>${ownedItem.name}</strong>?
        </center>
        <br>
      </div>
    </form>`;
      await Dialog.confirm({
        title: 'Deletar',
        content: template,
        yes: async () => {
          await ownedItem.delete();
          li.slideUp(200, () => this.render(false));
        },
        no: () => { },
      });
    });

    if (this.actor.isOwner) {
      //Rollable abilities.
      html.find('.roll-atributo').click(this._onRollAtributo.bind(this));

      //Rollable talentos.
      html.find('.roll-talento').click(this._onRollTalentos.bind(this));

      //Rollable ataque furtivo.
      html.find('.roll-furtivo').click(this._onRollAtaqueFurtivo.bind(this));

      //Rollable afastar mortos vivos.
      html.find('.roll-afastarmv').click(this._onRollAfastarMV.bind(this));

      //Rollable aprender magia.
      html.find('.roll-aprendermagia').click(this._onRollAprenderMagia.bind(this));

      //Rolar Surpresa
      html.find('.roll-surpresa').click(this._onRollSurpresa.bind(this));
      html.find('label.roll-surpresa').each((i, li) => {
        // Add draggable attribute and dragstart listener.
        li.setAttribute('draggable', true)
        li.addEventListener('dragstart', dragHandler, false)
      });

      //Rolar JP
      html.find('.roll-jp').click(this._onRollJP.bind(this));

      //Rollable Ataque improvisado.
      html.find('.roll-ataqueimprovisado').click(this._onRollAtaqueImprovisado.bind(this));

      //Rolar item
      html.find('.item-roll').click(this._onRollItem.bind(this));

      html.find(".item-controls .item-show").click(async (ev) => {
        const li = $(ev.currentTarget).parents(".item");
        const item = this.actor.items.get(li.data("itemId"));
        item.show();
      });

    }

    // Drag events for macros.
    if (this.actor.isOwner) {
      let handler = ev => this._onDragStart(ev);
      //let handler = ev => this._onDragItemStart(ev);
      // Find all items on the character sheet.
      html.find('li.item').each((i, li) => {
        // Ignore for the header row.
        if (li.classList.contains("item-header")) return;
        // Add draggable attribute and dragstart listener.
        li.setAttribute("draggable", true);
        li.addEventListener("dragstart", handler, false);
      });
    }

  }

  /** @override */
  _onDragStart(event){
    super._onDragStart(event);
    
  }

  /* -------------------------------------------- */
  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */

  _onRollAtributo(event) {
    event.preventDefault();
    let actorObject = this.actor;
    let element = event.currentTarget;
    let atributo = element.parentElement.dataset.key;

    actorObject.rollAtributos(atributo, { event: event });

  }

  _onRollItem(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);
    const element = event.currentTarget;
    const dataset = element.dataset;

    let roll = new Roll(dataset.roll, this.actor.system);
    item.roll(roll);
  }

  _onItemShow(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);
    const element = event.currentTarget;
    const dataset = element.dataset;

    item.show();
  }

  _onRollJP(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    let actorObject = this.actor;
    const jp = actorObject.system.jp;
    const label = dataset.label;

    actorObject.rollJP(label, jp, { event: event, type: dataset.type });
  }

  _onRollSurpresa(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    let actorObject = this.actor;
    let label = dataset.label ? `Rolando ${dataset.label}` : '';

    actorObject.rollSurpresa(label, { event: event });
  }

  _onRollTalentos(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    let actorObject = this.actor;
    let valor = actorObject.system.talento[dataset.key].value;
    let turnos = actorObject.system.talento[dataset.key].turnos;
    let habilidade = dataset.label;
    let label = `${dataset.verbo} ${dataset.label}`

    actorObject.rollTalentos(label, valor, habilidade, turnos, { event: event });
  }

  _onRollAfastarMV(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    let actorObject = this.actor;

    let level = actorObject.system.nivel;
    let afastarmv = actorObject.system.talento.afastarmv;

    actorObject.rollAfastarMV(level, afastarmv, { event: event, type: dataset.type });

  }

  _onRollAprenderMagia(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    let actorObject = this.actor;
    let valor = actorObject.system.talento.acesso_magia.aprender_chance;
    let label = `${dataset.verbo} ${dataset.label}`

    actorObject.rollAprenderMagia(label, valor, { event: event });
  }

  _onRollAtaqueImprovisado(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    let actorObject = this.actor;
    let bonusfinal;
    if (dataset.type == "ataquecac" || dataset.type == "ataquedesarmado" || dataset.type == "toquecac") {
      bonusfinal = actorObject.system.bonuscac;
    } else {
      bonusfinal = actorObject.system.bonusdist;
    }
    let label = dataset.label;
    actorObject.rollAtaqueImprovidado(label, bonusfinal, { event: event, type: dataset.type });
  }

  _onRollAtaqueFurtivo(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    let actorObject = this.actor;
    let multiplicador = actorObject.system.talento.ataque_furtivo.multiplicador;
    let label = dataset.label;

    actorObject.rollAtaqueFurtivo(label, multiplicador, { event: event, type: dataset.type });
  }

  _onToggleEquipped(event) {
    //console.log(item);
    event.preventDefault();
    const itemID = event.currentTarget.closest('.item').dataset.itemId;
    const item = this.actor.items.get(itemID);
    const equipado = !!item.system.equipado;
    return item.update({'system.equipado': !equipado});
  }

  _onToggleMemorizada(event) {
    //console.log(item);
    event.preventDefault();
    const itemID = event.currentTarget.closest('.item').dataset.itemId;
    const item = this.actor.items.get(itemID);
    const memorizada = !!item.system.memorizada;
    return item.update({'system.memorizada': !memorizada});
  }
  
}
