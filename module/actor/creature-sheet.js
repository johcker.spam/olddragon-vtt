export class OldDragonCreatureSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["olddragon", "sheet", "actor", "creature"],
      template: "systems/olddragon/templates/actor/creature-sheet.html",
      width: 840,
      height: 780,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "principal" }]
    });
  }

  /** @override */
  getData() {
    // const data = super.getData();
    // data.dtypes = ["String", "Number", "Boolean"];

    let isOwner = this.actor.isOwner;
    const data = {
      owner: isOwner,
      limited: this.actor.limited,
      options: this.options,
      editable: this.isEditable,
      cssClass: isOwner ? "editable" : "locked",
      isCharacter: this.actor.type === "creature",
      rollData: this.actor.getRollData.bind(this.actor)
    };

    // The Actor's data
    //const actorData = this.actor.data.toObject(false);
    data.actor = duplicate(this.document);
    data.actor.name = this.document.name;
    data.system = duplicate(this.document.system);
    data.labels = this.document.labels || {}
    data.filters = this._filters

    // Owned Items
    data.items = data.actor.items;
    for ( let i of data.items ) {
      const item = this.actor.items.get(i._id);
      i.labels = item.labels;
    }
    data.items.sort((a, b) => (a.sort || 0) - (b.sort || 0));

    if (this.actor.data.type == 'creature') {
      this._prepareCharacterItems(data);
    }

    return data;
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareCharacterItems(sheetData) {
    const actorData = sheetData;

    const ataques = [];
    const magias = [];

    for (let i of sheetData.items) {
      let item = i.system;
      i.img = i.img || DEFAULT_TOKEN;

      if (i.type === 'ataque') {
        ataques.push(i);
      }
      else if (i.type === 'magia') {
        magias.push(i);
      }

    }

    actorData.magias = magias;
    actorData.ataques = ataques;

  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    const dragHandler = ev => this._onDragStart(ev)


    html.find('.item-create').click(async (event) => {
      event.preventDefault();
      const header = event.currentTarget;
      let type = header.dataset.type;
      let createdItem;
      // item creation helper func
      let createItem = function (type, name = `Novo(a) ${type.capitalize()}`) {
        const itemData = {
          name: name ? name : `Novo(a) ${type.capitalize()}`,
          type: type,
          data: duplicate(header.dataset),
        };
        delete itemData.data['type'];
        return itemData;
      };
      const itemData = createItem(type);
      createdItem = await Item.create(itemData, {parent: this.actor});
      // createdItem = await this.actor.createOwnedItem(itemData, {});
      this.actor.items.get(createdItem.id).sheet.render(true);

    });

    //Toggle Magic
    html.find('.item-togglemagic').click(this._onToggleMemorizada.bind(this));


    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Item
    html.find('.item-delete').click(async (ev) => {
      const li = $(ev.currentTarget).parents('.item');
      let ownedItem = this.actor.items.get(li.data('itemId'));
      const template = `
    <form>
      <div>
        <center>Deletar 
          <strong>${ownedItem.name}</strong>?
        </center>
        <br>
      </div>
    </form>`;
      await Dialog.confirm({
        title: 'Deletar',
        content: template,
        yes: async () => {
          await ownedItem.delete();
          li.slideUp(200, () => this.render(false));
        },
        no: () => { },
      });
    });

    if(this.actor.isOwner){
      //Rollable abilities.
      html.find('.roll-atributo').click(this._onRollAtributo.bind(this));

    }

    //Rolar Surpresa
    html.find('.roll-surpresa').click(this._onRollSurpresa.bind(this));
    html.find('label.roll-surpresa').each((i, li) => {
      // Add draggable attribute and dragstart listener.
      li.setAttribute('draggable', true)
      li.addEventListener('dragstart', dragHandler, false)
    });

    //Rolar Surpresa
    html.find('.roll-moral').click(this._onRollMoral.bind(this));

    //Rolar JP
    html.find('.roll-jp').click(this._onRollJP.bind(this));

    //Rolar HP
    html.find('.roll-hp').click(this._onRollHP.bind(this));

    //Rolar item
    html.find('.item-roll').click(this._onRollItem.bind(this));

    html.find(".item-controls .item-show").click(async (ev) => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      item.show();
    });

  }


  /* -------------------------------------------- */
  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */

  _onRollAtributo(event) {
    event.preventDefault();
    let actorObject = this.actor;
    let element = event.currentTarget;
    let atributo = element.parentElement.dataset.key;

    actorObject.rollAtributos(atributo, {event: event});

  }

  _onRollItem(event){
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);
    const element = event.currentTarget;
    const dataset = element.dataset;

    let roll = new Roll(dataset.roll, this.actor.system);
    item.roll(roll);
  }

  _onItemShow(event){
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);
    const element = event.currentTarget;
    const dataset = element.dataset;

    item.show();
  }

  _onRollJP(event){
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    let actorObject = this.actor;
    const jp = actorObject.system.jp;
    const label = dataset.label;

    actorObject.rollJP(label, jp, {event: event, type: dataset.type});
  }

  _onRollHP(event){
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    let actorObject = this.actor;
    const label = dataset.label;

    actorObject.rollHP(label, {event: event, type: dataset.type});
  }

  _onRollSurpresa(event){
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    let actorObject = this.actor;
    let label = dataset.label ? `Rolando ${dataset.label}` : '';

    actorObject.rollSurpresa(label, {event: event});
  }

  _onRollMoral(event){
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;
    let actorObject = this.actor;
    let label = dataset.label ? `Verificando ${dataset.label}` : '';

    actorObject.rollMoral(label, {event: event});
  }

  _onToggleEquipped(event) {
    //console.log(item);
    event.preventDefault();
    const itemID = event.currentTarget.closest('.item').dataset.itemId;
    const item = this.actor.items.get(itemID);
    const equipado = !!item.system.equipado;
    return item.update({'data.equipado': !equipado});
  }

  _onToggleMemorizada(event) {
    //console.log(item);
    event.preventDefault();
    const itemID = event.currentTarget.closest('.item').dataset.itemId;
    const item = this.actor.items.get(itemID);
    const memorizada = !!item.system.memorizada;
    return item.update({'data.memorizada': !memorizada});
  }

}