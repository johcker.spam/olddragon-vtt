/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
import { ODDice } from "../dice.js";

export class OldDragonActor extends Actor {

    /**
     * Augment the basic actor data with additional dynamic data.
     */
    prepareBaseData() {
      super.prepareBaseData();
  
    //   const actorData = this.data;
    //   const data = actorData.data;
    //   const flags = actorData.flags;

      const actorData = this;
      const data = this.system;
      const flags = this.flags;
  
      // Make separate methods for each Actor type (character, npc, etc.) to keep
      // things organized.
      this._prepareCharacterData(actorData);

      if (actorData.type === 'character'){
        //preparar ac dos equipamentos
        data.ac.dex = data.atributos.dex.mod;
        data.ac.value = data.ac.base + data.ac.dex + data.ac.armadura + data.ac.escudo + data.ac.outros; 
        
        data.bonuscac = data.ba.primario + data.atributos.str.mod;
        data.bonusdist = data.ba.primario + data.atributos.dex.mod;

        //Idiomas adicionais e aprendizado de magia
      let idiomasval = 0; 
      let intelval = data.atributos.int.value;
      
      if (intelval >= 8 && intelval <= 15){
          idiomasval = 1;
      } else if (intelval >=16 && intelval <=29){
          idiomasval = (Math.floor((intelval - 10)/2)) - 1;
      }
      
      let learnval = 0;
      
      if (intelval >=8 && intelval <= 9){
          learnval = 5;
      } else if (intelval >=10 && intelval <= 11){
          learnval = 10;
      } else if (intelval >=12 && intelval <= 13){
          learnval = 20;
      } else if (intelval >=14 && intelval <= 29){
          learnval = (data.atributos.int.mod) * 10 + 5;
      }
      data.idiomas.adicionais = idiomasval;
      data.talento.acesso_magia.aprender_chance = learnval;

      data.seguidores.max = data.atributos.cha.mod >= 0 ? (data.atributos.cha.mod + 1): 0;
  
      // Chance de Ressurreição
      let constival = data.atributos.con.value;
      let chanceres = 0;
      
      if(constival <= 5){
          chanceres = 0;
      } else if (constival >= 6 && constival <= 7){
          chanceres = 1;
      } else if (constival >= 8 && constival <= 9){
          chanceres = 5;
      } else if (constival >= 10 && constival <= 11){
          chanceres = 10;
      } else if (constival >= 12 && constival <= 13){
          chanceres = 25;
      } else if (constival >= 14 && constival <= 15){
          chanceres = 50;
      } else if (constival >= 16 && constival <= 17){
          chanceres = 75;
      } else if (constival >= 18 && constival <= 19){
          chanceres = 95;
      } else if (constival >= 20){
          chanceres = 100;
      }
  
      data.hp.ressurreicao = chanceres;
  
      //Quantidade Afastada
  
      let charival = data.atributos.cha.value;
      let qtdrepeal = "0";
      
      if(charival >= 28) qtdrepeal = "1d20";
      else if (charival >= 26) qtdrepeal = "2d6";
      else if (charival >= 24) qtdrepeal = "1d12";
      else if (charival >= 22) qtdrepeal = "1d10";
      else if (charival >= 20) qtdrepeal = "2d4";
      else if (charival >= 18) qtdrepeal = "1d8";
      else if (charival >= 16) qtdrepeal = "1d6";
      else if (charival >= 14) qtdrepeal = "1d4";
      else if (charival >= 12) qtdrepeal = "1d3";
      else if (charival >= 10) qtdrepeal = "1d2";
      else if (charival >= 8) qtdrepeal =  "1";

      data.talento.afastarmv.qtd_afastada = qtdrepeal;
        
      //Definição de carga
  
      let strval = data.atributos.str.value;
      let cargaleve = 0;
      let cargamedia = 0;
      let cargapesada = 0;
      
      if (strval >= 21) { cargaleve = 90; cargamedia = 130; cargapesada = 150; }
      else if (strval >= 18) { cargaleve = 60; cargamedia = 100; cargapesada = 120; }
      else if (strval >= 16) { cargaleve = 50; cargamedia = 80; cargapesada = 100; }
      else if (strval >= 13) { cargaleve = 40; cargamedia = 70; cargapesada = 90; }
      else if (strval >= 9) { cargaleve = 30; cargamedia = 50; cargapesada = 70; }
      else if (strval >= 4) { cargaleve = 20; cargamedia = 30; cargapesada = 50; }
      else if (strval = 3) { cargaleve = 10; cargamedia = 20; cargapesada = 30; }
  
      data.carga.leve = cargaleve;
      data.carga.media = cargamedia;
      data.carga.pesada = cargapesada;
      data.carga.atual = this._calculaCargaAtual(actorData);
      data.velocidade.value = data.velocidade.raca + data.velocidade.armadura + data.velocidade.carga;

    // Cálculo de carga
    //   if (data.carga.atual > data.carga.pesada) {
    //     data.velocidade.value = 1;
    //     data.velocidade.carga = "X";
    //   } else if (data.carga.atual > data.carga.media) {
    //     data.velocidade.carga = -2;
    //     data.velocidade.value = data.velocidade.raca + data.velocidade.armadura + data.velocidade.carga;
    //   } else if (data.carga.atual > data.carga.leve) {
    //     data.velocidade.value = data.velocidade.raca + data.velocidade.armadura + data.velocidade.carga;
    //     data.velocidade.carga = -1;
    //   } else {
    //     data.velocidade.carga = 0;
    //     data.velocidade.value = data.velocidade.raca + data.velocidade.armadura + data.velocidade.carga;
    //   }

      


      data.riqueza.valor = (data.riqueza.ppl * 100) + (data.riqueza.pe * 10) + (data.riqueza.po * 1) + (data.riqueza.pp * 0.1) + (data.riqueza.ppl * 0.01);
  
      //Círculos de magias
      let lvlval = data.nivel;
      let tipomagiasval = data.talento.acesso_magia.tipo_magia;
      let intelligenceval = data.atributos.int.value;
      let wisdomval = data.atributos.wis.value;
      let adic1 = 0;
      let adic2 = 0;
      let adic3 = 0;
      let c1 = 0;
      let c2 = 0;
      let c3 = 0;
      let c4 = 0;
      let c5 = 0;
      let c6 = 0;
      let c7 = 0;
      let c8 = 0;
      let c9 = 0;
      
      const arcanas = {
          lvl_1: {circ1: 1, circ2: 0, circ3: 0, circ4: 0, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_2: {circ1: 2, circ2: 0, circ3: 0, circ4: 0, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_3: {circ1: 2, circ2: 1, circ3: 0, circ4: 0, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_4: {circ1: 2, circ2: 2, circ3: 0, circ4: 0, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_5: {circ1: 3, circ2: 2, circ3: 1, circ4: 0, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_6: {circ1: 3, circ2: 2, circ3: 2, circ4: 0, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_7: {circ1: 3, circ2: 2, circ3: 2, circ4: 1, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_8: {circ1: 3, circ2: 3, circ3: 2, circ4: 2, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_9: {circ1: 3, circ2: 3, circ3: 2, circ4: 2, circ5: 1, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_10: {circ1: 3, circ2: 3, circ3: 3, circ4: 2, circ5: 2, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_11: {circ1: 4, circ2: 3, circ3: 3, circ4: 2, circ5: 2, circ6: 1, circ7: 0, circ8: 0, circ9: 0},
          lvl_12: {circ1: 4, circ2: 3, circ3: 3, circ4: 3, circ5: 2, circ6: 2, circ7: 0, circ8: 0, circ9: 0},
          lvl_13: {circ1: 4, circ2: 4, circ3: 3, circ4: 3, circ5: 2, circ6: 2, circ7: 1, circ8: 0, circ9: 0},
          lvl_14: {circ1: 4, circ2: 4, circ3: 3, circ4: 3, circ5: 3, circ6: 2, circ7: 2, circ8: 0, circ9: 0},
          lvl_15: {circ1: 5, circ2: 4, circ3: 4, circ4: 3, circ5: 3, circ6: 2, circ7: 2, circ8: 1, circ9: 0},
          lvl_16: {circ1: 5, circ2: 4, circ3: 4, circ4: 3, circ5: 3, circ6: 3, circ7: 2, circ8: 2, circ9: 0},
          lvl_17: {circ1: 5, circ2: 5, circ3: 4, circ4: 4, circ5: 3, circ6: 3, circ7: 2, circ8: 2, circ9: 1},
          lvl_18: {circ1: 5, circ2: 5, circ3: 4, circ4: 4, circ5: 3, circ6: 3, circ7: 3, circ8: 2, circ9: 2},
          lvl_19: {circ1: 5, circ2: 5, circ3: 5, circ4: 4, circ5: 4, circ6: 3, circ7: 3, circ8: 2, circ9: 2},
          lvl_20: {circ1: 6, circ2: 6, circ3: 5, circ4: 4, circ5: 4, circ6: 3, circ7: 3, circ8: 3, circ9: 2}
      }
      
      const divinas = {
          lvl_1: {circ1: 1, circ2: 0, circ3: 0, circ4: 0, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_2: {circ1: 2, circ2: 0, circ3: 0, circ4: 0, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_3: {circ1: 2, circ2: 1, circ3: 0, circ4: 0, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_4: {circ1: 3, circ2: 2, circ3: 0, circ4: 0, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_5: {circ1: 3, circ2: 2, circ3: 1, circ4: 0, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_6: {circ1: 3, circ2: 3, circ3: 2, circ4: 0, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_7: {circ1: 4, circ2: 3, circ3: 2, circ4: 1, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_8: {circ1: 4, circ2: 3, circ3: 3, circ4: 2, circ5: 0, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_9: {circ1: 4, circ2: 4, circ3: 3, circ4: 2, circ5: 1, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_10: {circ1: 5, circ2: 4, circ3: 3, circ4: 3, circ5: 2, circ6: 0, circ7: 0, circ8: 0, circ9: 0},
          lvl_11: {circ1: 5, circ2: 4, circ3: 4, circ4: 3, circ5: 2, circ6: 1, circ7: 0, circ8: 0, circ9: 0},
          lvl_12: {circ1: 5, circ2: 5, circ3: 4, circ4: 3, circ5: 3, circ6: 2, circ7: 0, circ8: 0, circ9: 0},
          lvl_13: {circ1: 6, circ2: 5, circ3: 4, circ4: 4, circ5: 3, circ6: 2, circ7: 0, circ8: 0, circ9: 0},
          lvl_14: {circ1: 6, circ2: 5, circ3: 5, circ4: 4, circ5: 3, circ6: 3, circ7: 0, circ8: 0, circ9: 0},
          lvl_15: {circ1: 7, circ2: 6, circ3: 5, circ4: 4, circ5: 4, circ6: 3, circ7: 1, circ8: 0, circ9: 0},
          lvl_16: {circ1: 7, circ2: 6, circ3: 5, circ4: 5, circ5: 4, circ6: 3, circ7: 2, circ8: 0, circ9: 0},
          lvl_17: {circ1: 8, circ2: 7, circ3: 6, circ4: 5, circ5: 4, circ6: 4, circ7: 2, circ8: 0, circ9: 0},
          lvl_18: {circ1: 8, circ2: 7, circ3: 6, circ4: 5, circ5: 5, circ6: 4, circ7: 3, circ8: 0, circ9: 0},
          lvl_19: {circ1: 9, circ2: 8, circ3: 7, circ4: 6, circ5: 5, circ6: 4, circ7: 3, circ8: 0, circ9: 0},
          lvl_20: {circ1: 9, circ2: 8, circ3: 7, circ4: 6, circ5: 5, circ6: 5, circ7: 3, circ8: 0, circ9: 0}
      }
      
      if(tipomagiasval == "arcana"){
          
          c1 = arcanas[("lvl_"+lvlval)].circ1;
          c2 = arcanas[("lvl_"+lvlval)].circ2;
          c3 = arcanas[("lvl_"+lvlval)].circ3;
          c4 = arcanas[("lvl_"+lvlval)].circ4;
          c5 = arcanas[("lvl_"+lvlval)].circ5;
          c6 = arcanas[("lvl_"+lvlval)].circ6;
          c7 = arcanas[("lvl_"+lvlval)].circ7;
          c8 = arcanas[("lvl_"+lvlval)].circ8;
          c9 = arcanas[("lvl_"+lvlval)].circ9;
          
          if (intelligenceval >= 28) { adic1 = 3; adic2 = 3; adic3 = 1; }
          else if (intelligenceval >= 26) { adic1 = 3; adic2 = 2; adic3 = 1; }
          else if (intelligenceval >= 24) { adic1 = 2; adic2 = 2; adic3 = 1; }
          else if (intelligenceval >= 22) { adic1 = 2; adic2 = 2; }
          else if (intelligenceval >= 20) { adic1 = 2; adic2 = 1; }
          else if (intelligenceval >= 18) { adic1 = 2; }
          else if (intelligenceval >= 16) { adic1 = 1; }
          
      } else if (tipomagiasval == "divina") {
          
          c1 = divinas[("lvl_"+lvlval)].circ1;
          c2 = divinas[("lvl_"+lvlval)].circ2;
          c3 = divinas[("lvl_"+lvlval)].circ3;
          c4 = divinas[("lvl_"+lvlval)].circ4;
          c5 = divinas[("lvl_"+lvlval)].circ5;
          c6 = divinas[("lvl_"+lvlval)].circ6;
          c7 = divinas[("lvl_"+lvlval)].circ7;
          c8 = divinas[("lvl_"+lvlval)].circ8;
          c9 = divinas[("lvl_"+lvlval)].circ9;
  
          if (wisdomval >= 28) { adic1 = 3; adic2 = 3; adic3 = 1; }
          else if (wisdomval >= 26) { adic1 = 3; adic2 = 2; adic3 = 1; }
          else if (wisdomval >= 24) { adic1 = 2; adic2 = 2; adic3 = 1; }
          else if (wisdomval >= 22) { adic1 = 2; adic2 = 2; }
          else if (wisdomval >= 20) { adic1 = 2; adic2 = 1; }
          else if (wisdomval >= 18) { adic1 = 2; }
          else if (wisdomval >= 16) { adic1 = 1; }
      }
  
      data.talento.afastarmv.limite_max = lvlval;
  
      data.talento.acesso_magia.circ1 = c1;
      data.talento.acesso_magia.circ2 = c2;
      data.talento.acesso_magia.circ3 = c3;
      data.talento.acesso_magia.circ4 = c4;
      data.talento.acesso_magia.circ5 = c5;
      data.talento.acesso_magia.circ6 = c6;
      data.talento.acesso_magia.circ7 = c7;
      data.talento.acesso_magia.circ8 = c8;
      data.talento.acesso_magia.circ9 = c9;
  
      data.talento.acesso_magia.adic1 = adic1;
      data.talento.acesso_magia.adic2 = adic2;
      data.talento.acesso_magia.adic3 = adic3;
  
  
      //Talendo de ladrão e Afastar mortos vivos
  
      const talentos = {
          lvl_1: {arrombar: 15, arrombardx: "1d8", armadilha:10, armadilhadx:"1d8", escalar:80, furtividade:20, punga:20, percepcao: 2, ataquefurt: 2},
          lvl_2: {arrombar: 20, arrombardx: "1d8", armadilha:15, armadilhadx:"1d8", escalar:81, furtividade:25, punga:25, percepcao: 2, ataquefurt: 2},
          lvl_3: {arrombar: 25, arrombardx: "1d8", armadilha:20, armadilhadx:"1d8", escalar:82, furtividade:30, punga:30, percepcao: 2, ataquefurt: 2},
          lvl_4: {arrombar: 30, arrombardx: "1d8", armadilha:25, armadilhadx:"1d8", escalar:83, furtividade:35, punga:35, percepcao: 2, ataquefurt: 2},
          lvl_5: {arrombar: 35, arrombardx: "1d8", armadilha:30, armadilhadx:"1d8", escalar:84, furtividade:40, punga:40, percepcao: 3, ataquefurt: 2},
          lvl_6: {arrombar: 40, arrombardx: "1d6", armadilha:35, armadilhadx:"1d6", escalar:85, furtividade:45, punga:45, percepcao: 3, ataquefurt: 3},
          lvl_7: {arrombar: 45, arrombardx: "1d6", armadilha:40, armadilhadx:"1d6", escalar:86, furtividade:50, punga:50, percepcao: 3, ataquefurt: 3},
          lvl_8: {arrombar: 50, arrombardx: "1d6", armadilha:45, armadilhadx:"1d6", escalar:87, furtividade:55, punga:55, percepcao: 3, ataquefurt: 3},
          lvl_9: {arrombar: 55, arrombardx: "1d6", armadilha:50, armadilhadx:"1d6", escalar:88, furtividade:60, punga:60, percepcao: 3, ataquefurt: 3},
          lvl_10: {arrombar: 60, arrombardx: "1d6", armadilha:55, armadilhadx:"1d6", escalar:89, furtividade:65, punga:65, percepcao: 4, ataquefurt: 3},
          lvl_11: {arrombar: 62, arrombardx: "1d4", armadilha:60, armadilhadx:"1d4", escalar:90, furtividade:70, punga:70, percepcao: 4, ataquefurt: 3},
          lvl_12: {arrombar: 64, arrombardx: "1d4", armadilha:62, armadilhadx:"1d4", escalar:91, furtividade:72, punga:72, percepcao: 4, ataquefurt: 4},
          lvl_13: {arrombar: 66, arrombardx: "1d4", armadilha:64, armadilhadx:"1d4", escalar:92, furtividade:74, punga:74, percepcao: 4, ataquefurt: 4},
          lvl_14: {arrombar: 68, arrombardx: "1d4", armadilha:66, armadilhadx:"1d4", escalar:93, furtividade:76, punga:76, percepcao: 4, ataquefurt: 4},
          lvl_15: {arrombar: 70, arrombardx: "1d4", armadilha:68, armadilhadx:"1d4", escalar:94, furtividade:78, punga:78, percepcao: 4, ataquefurt: 4},
          lvl_16: {arrombar: 72, arrombardx: "1", armadilha:70, armadilhadx:"1", escalar:95, furtividade:80, punga:80, percepcao: 5, ataquefurt: 4},
          lvl_17: {arrombar: 74, arrombardx: "1", armadilha:72, armadilhadx:"1", escalar:96, furtividade:82, punga:82, percepcao: 5, ataquefurt: 4},
          lvl_18: {arrombar: 76, arrombardx: "1", armadilha:74, armadilhadx:"1", escalar:97, furtividade:84, punga:84, percepcao: 5, ataquefurt: 5},
          lvl_19: {arrombar: 78, arrombardx: "1", armadilha:76, armadilhadx:"1", escalar:98, furtividade:86, punga:86, percepcao: 5, ataquefurt: 5},
          lvl_20: {arrombar: 80, arrombardx: "1", armadilha:78, armadilhadx:"1", escalar:99, furtividade:88, punga:88, percepcao: 5, ataquefurt: 5}
      };
      
      const afastarmv = {
          lvl_1: {dv1:"13",dv2:"17",dv3:"19",dv4:"N",dv5:"N",dv6:"N",dv7:"N",dv8:"N",dv9:"N",dv10:"N",dv11:"N",dv12:"N",dv13:"N",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_2: {dv1:"11",dv2:"15",dv3:"18",dv4:"20",dv5:"N",dv6:"N",dv7:"N",dv8:"N",dv9:"N",dv10:"N",dv11:"N",dv12:"N",dv13:"N",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_3: {dv1:"9",dv2:"13",dv3:"17",dv4:"19",dv5:"N",dv6:"N",dv7:"N",dv8:"N",dv9:"N",dv10:"N",dv11:"N",dv12:"N",dv13:"N",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_4: {dv1:"7",dv2:"11",dv3:"15",dv4:"18",dv5:"20",dv6:"N",dv7:"N",dv8:"N",dv9:"N",dv10:"N",dv11:"N",dv12:"N",dv13:"N",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_5: {dv1:"5",dv2:"9",dv3:"13",dv4:"17",dv5:"19",dv6:"N",dv7:"N",dv8:"N",dv9:"N",dv10:"N",dv11:"N",dv12:"N",dv13:"N",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_6: {dv1:"3",dv2:"7",dv3:"11",dv4:"15",dv5:"18",dv6:"20",dv7:"N",dv8:"N",dv9:"N",dv10:"N",dv11:"N",dv12:"N",dv13:"N",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_7: {dv1:"A",dv2:"5",dv3:"9",dv4:"13",dv5:"17",dv6:"19",dv7:"N",dv8:"N",dv9:"N",dv10:"N",dv11:"N",dv12:"N",dv13:"N",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_8: {dv1:"A",dv2:"3",dv3:"7",dv4:"11",dv5:"15",dv6:"18",dv7:"20",dv8:"N",dv9:"N",dv10:"N",dv11:"N",dv12:"N",dv13:"N",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_9: {dv1:"A",dv2:"2",dv3:"5",dv4:"9",dv5:"13",dv6:"17",dv7:"19",dv8:"N",dv9:"N",dv10:"N",dv11:"N",dv12:"N",dv13:"N",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_10:{dv1:"D",dv2:"A",dv3:"3",dv4:"7",dv5:"11",dv6:"15",dv7:"18",dv8:"20",dv9:"N",dv10:"N",dv11:"N",dv12:"N",dv13:"N",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_11:{dv1:"D",dv2:"A",dv3:"2",dv4:"5",dv5:"9",dv6:"13",dv7:"17",dv8:"19",dv9:"20",dv10:"N",dv11:"N",dv12:"N",dv13:"N",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_12:{dv1:"D",dv2:"A",dv3:"A",dv4:"3",dv5:"7",dv6:"11",dv7:"15",dv8:"18",dv9:"19",dv10:"20",dv11:"N",dv12:"N",dv13:"N",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_13:{dv1:"D",dv2:"D",dv3:"A",dv4:"2",dv5:"5",dv6:"9",dv7:"13",dv8:"17",dv9:"18",dv10:"19",dv11:"20",dv12:"N",dv13:"N",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_14:{dv1:"D",dv2:"D",dv3:"A",dv4:"A",dv5:"3",dv6:"7",dv7:"11",dv8:"15",dv9:"17",dv10:"18",dv11:"19",dv12:"20",dv13:"N",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_15:{dv1:"D",dv2:"D",dv3:"D",dv4:"A",dv5:"2",dv6:"5",dv7:"9",dv8:"13",dv9:"15",dv10:"17",dv11:"18",dv12:"19",dv13:"20",dv14:"N",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_16:{dv1:"D",dv2:"D",dv3:"D",dv4:"A",dv5:"A",dv6:"3",dv7:"7",dv8:"11",dv9:"13",dv10:"15",dv11:"17",dv12:"18",dv13:"19",dv14:"20",dv15:"N",dv16:"N",dv17:"N",dv18:"N",},
          lvl_17:{dv1:"D",dv2:"D",dv3:"D",dv4:"D",dv5:"A",dv6:"2",dv7:"5",dv8:"9",dv9:"11",dv10:"13",dv11:"15",dv12:"17",dv13:"18",dv14:"19",dv15:"20",dv16:"N",dv17:"N",dv18:"N",},
          lvl_18:{dv1:"D",dv2:"D",dv3:"D",dv4:"D",dv5:"A",dv6:"A",dv7:"3",dv8:"7",dv9:"9",dv10:"11",dv11:"13",dv12:"15",dv13:"17",dv14:"18",dv15:"19",dv16:"20",dv17:"N",dv18:"N",},
          lvl_19:{dv1:"D",dv2:"D",dv3:"D",dv4:"D",dv5:"D",dv6:"A",dv7:"2",dv8:"5",dv9:"7",dv10:"9",dv11:"11",dv12:"13",dv13:"15",dv14:"17",dv15:"18",dv16:"19",dv17:"20",dv18:"N",},
          lvl_20:{dv1:"D",dv2:"D",dv3:"D",dv4:"D",dv5:"D",dv6:"A",dv7:"A",dv8:"3",dv9:"5",dv10:"7",dv11:"9",dv12:"11",dv13:"13",dv14:"15",dv15:"17",dv16:"18",dv17:"19",dv18:"20"}
      };
  
      let dexval = data.atributos.dex.value;
      let dexmod = data.atributos.dex.mod;
      //let lvlval = values.level;
      let tarrombar = talentos[("lvl_"+lvlval)].arrombar;
      let tarrombardx = talentos[("lvl_"+lvlval)].arrombardx;
      let tarmadilha = talentos[("lvl_"+lvlval)].armadilha;
      let tarmadilhadx = talentos[("lvl_"+lvlval)].armadilhadx;
      let tescalar = talentos[("lvl_"+lvlval)].escalar;
      let tfurtividade = talentos[("lvl_"+lvlval)].furtividade;
      let tpunga = talentos[("lvl_"+lvlval)].punga;
      let tpercepcao = talentos[("lvl_"+lvlval)].percepcao;
      let tataquefurt = talentos[("lvl_"+lvlval)].ataquefurt;
      
      let d1 = afastarmv[("lvl_"+lvlval)].dv1;
      let d2 = afastarmv[("lvl_"+lvlval)].dv2;
      let d3 = afastarmv[("lvl_"+lvlval)].dv3;
      let d4 = afastarmv[("lvl_"+lvlval)].dv4;
      let d5 = afastarmv[("lvl_"+lvlval)].dv5;
      let d6 = afastarmv[("lvl_"+lvlval)].dv6;
      let d7 = afastarmv[("lvl_"+lvlval)].dv7;
      let d8 = afastarmv[("lvl_"+lvlval)].dv8;
      let d9 = afastarmv[("lvl_"+lvlval)].dv9;
      let d10 = afastarmv[("lvl_"+lvlval)].dv10;
      let d11 = afastarmv[("lvl_"+lvlval)].dv11;
      let d12 = afastarmv[("lvl_"+lvlval)].dv12;
      let d13 = afastarmv[("lvl_"+lvlval)].dv13;
      let d14 = afastarmv[("lvl_"+lvlval)].dv14;
      let d15 = afastarmv[("lvl_"+lvlval)].dv15;
      let d16 = afastarmv[("lvl_"+lvlval)].dv16;
      let d17 = afastarmv[("lvl_"+lvlval)].dv17;
      let d18 = afastarmv[("lvl_"+lvlval)].dv18;
      
      
      let uarrombar = dexmod * 5;
      let uarmadilha = 0;
      let ufurtpunga = 0;
      
      if(dexval >= 14) { 
          uarmadilha = (dexmod * 5) - 10; 
          ufurtpunga = (dexmod * 5) - 5  
          
      } else if (dexval >= 12) {
          uarmadilha = (dexmod * 5) - 5; 
          ufurtpunga = (dexmod * 5) - 5
          
      } else if (dexval >= 1) { 
          uarmadilha = (dexmod * 5); 
          ufurtpunga = (dexmod * 5) 
      }
      
      data.talento.arrombar.value = (tarrombar+uarrombar);
      data.talento.arrombar.turnos = tarrombardx;
  
      data.talento.armadilhas.value = (tarmadilha+uarmadilha);
      data.talento.armadilhas.turnos = tarmadilhadx;
  
      data.talento.escalar.value = tescalar;
  
      data.talento.furtividade.value = (tfurtividade+ufurtpunga);
  
      data.talento.pungar.value = (tpunga+ufurtpunga);
  
      data.talento.percepcao.value = tpercepcao;
  
      data.talento.ataque_furtivo.multiplicador = tataquefurt;
  
      data.talento.afastarmv.dv1 = d1;
      data.talento.afastarmv.dv2 = d2;
      data.talento.afastarmv.dv3 = d3;
      data.talento.afastarmv.dv4 = d4;
      data.talento.afastarmv.dv5 = d5;
      data.talento.afastarmv.dv6 = d6;
      data.talento.afastarmv.dv7 = d7;
      data.talento.afastarmv.dv8 = d8;
      data.talento.afastarmv.dv9 = d9;
      data.talento.afastarmv.dv10 = d10;
      data.talento.afastarmv.dv11 = d11;
      data.talento.afastarmv.dv12 = d12;
      data.talento.afastarmv.dv13 = d13;
      data.talento.afastarmv.dv14 = d14;
      data.talento.afastarmv.dv15 = d15;
      data.talento.afastarmv.dv16 = d16;
      data.talento.afastarmv.dv17 = d17;
      data.talento.afastarmv.dv18 = d18;

      } 

    }
  
    /**
     * Prepare Character type specific data
     */
    _prepareCharacterData(actorData) {
      //console.log(actorData);
      const data = actorData.system;
      // Make modifications to data here. For example:
  
      // Loop through ability scores, and add their modifiers to our sheet output.
      for (let [key, atributo] of Object.entries(data.atributos)) {
        // Calculate the modifier using d20 rules.
        atributo.mod = Math.floor((atributo.value - 10) / 2);
      }
  
      data.jp.dex.value = data.atributos.dex.mod;
      data.jp.con.value = data.atributos.con.mod;
      data.jp.wis.value = data.atributos.wis.mod;

      //console.log(data)
    }

    //value="(@{inventory-platinum} * 100)+(@{inventory-electrum} * 10)+(@{inventory-gold} * 1)+(@{inventory-silver} * 0.1)+(@{inventory-copper} * 0.01)"


    _calculaCargaAtual(actorData){
        //console.log(actorData);
        let total = 0;
        let itensCarregados = actorData.items.filter(
            (i) => i.type != "magia" && i.type != "pergaminho"
          );
          itensCarregados.forEach((item) => {
            if(item.system?.quantidade > 1){
                item.system.peso_total = item.system.peso * item.system?.quantidade;
                total += item.system.peso_total;
            } else {
                item.system.peso_total = item.system.peso;
                total += item.system.peso_total;
            }
            
          });
        let moedasCarregadas = this.system.riqueza.ppl + this.system.riqueza.pe 
                    + this.system.riqueza.po + this.system.riqueza.pp 
                    + this.system.riqueza.pc ;
        this.system.riqueza.peso = moedasCarregadas * 0.01

        total += this.system.riqueza.peso;

        return total;

    }

    /* -------------------------------------------- */
    /*  Rolls                                       */
    /* -------------------------------------------- */
  
    rollSurpresa(label, options = {}){
        const rollParts = ["1d6", this.system.atributos.dex.mod];

        const data = {
            actor: this.system,
            roll: {
                type: "surpresa",
                target: 6
            },
            details: label
        };

        let skip = options.event && options.event.ctrlKey;

        // Roll and return
        return ODDice.Roll({
            event: options.event,
            parts: rollParts,
            data: data,
            skipDialog: skip,
            speaker: ChatMessage.getSpeaker({ actor: this }),
            flavor: label,
            title: label,
        });

    }

    rollMoral(label, options = {}){
        const rollParts = ["2d6"];

        const data = {
            actor: this.system,
            roll: {
                type: "moral",
                target: this.system.moral
            },
            details: label
        };

        let skip = true;

        // Roll and return
        return ODDice.Roll({
            event: options.event,
            parts: rollParts,
            data: data,
            skipDialog: skip,
            speaker: ChatMessage.getSpeaker({ actor: this }),
            flavor: label,
            title: label,
        });

    }

    rollJP(label, jp, options={}){
        const rollParts = ["1d20"];

        const data = {
            actor: this.system,
            roll: {
                type: options.type,
                target: jp.base.value
            },
            details: label,
            jp: jp
        };

        return ODDice.Roll({
            event:options.event,
            parts:rollParts,
            data:data,
            skipDialog: false,
            speaker: ChatMessage.getSpeaker({ actor: this }),
            flavor: label,
            title: label,
        });
        
    }

    rollHP(label, options={}){
        const arrayOfStrings = (this.system.dvs.d8).split('d');
        var modificador = (parseInt(arrayOfStrings[0], 10)) * this.system.atributos.con.mod;
        var ajuste = this.system.dvs.ajuste;
        // console.log(arrayOfStrings);
        const rollParts = [this.system.dvs.d8, modificador, ajuste];

        const data = {
            actor: this.system,
            roll: {
                type: "hpdv",
            },
            details: "Rolagem DVs + Mod. CON + Ajuste"
        };

        let skip = false;

        // Roll and return
        return ODDice.Roll({
            event: options.event,
            parts: rollParts,
            data: data,
            skipDialog: skip,
            speaker: ChatMessage.getSpeaker({ actor: this }),
            flavor: label,
            title: label,
        });


    }

    rollAfastarMV(level, afastarmv, options={}){
        const rollParts = ["1d20"];
        const data = {
            actor: this.system,
            roll: {
                type: options.type
            },
            level: level,
            afastarmv: afastarmv,
            details: "Tentando afastar mortos vivos..."
        }

        return ODDice.Roll({
            event:options.event,
            parts:rollParts,
            data:data,
            skipDialog: false,
            speaker: ChatMessage.getSpeaker({ actor: this }),
            flavor: "Tentando afastar",
            title: "Afastar Mortos Vivos",
        });

    }

    rollTalentos(label, valor, habilidade, turnos, options ={}){
        const msg = label;
        let rollParts; 
        if (habilidade == "Percepção"){
            rollParts = ["1d6"];
        } else {
            rollParts = ["1d100"];
        }
        const target = valor;
        let msgTurnos;
        let rollTurnos;
        
        if (turnos != null){
            rollTurnos = new Roll(turnos).evaluate({async:false}).total;
            msgTurnos = `Termina em ${rollTurnos} turnos`
        }
        

        const data = {
            actor: this.system,
            roll: {
                type: "taladino",
                target: target,
            },
           details: msg,
           msgTurnos: msgTurnos
        };
        
        let skip = options.event && options.event.ctrlKey;

        return ODDice.Roll({
            event: options.event,
            parts: rollParts,
            data: data,
            skipDialog: skip,
            speaker: ChatMessage.getSpeaker({actor:this}),
            flavor: msg, 
            title: msg,
        });
    }

    rollAtaqueFurtivo (label, multiplicador, options={}){
        const msg = label;
        let rollParts = ["1d20"];

        const data = {
            actor: this,
            actorData: this.system,
            roll: {
                type: options.type,
                multiplicador: multiplicador
            }
        };

        let skip = options.event && options.event.ctrlKey;

        return ODDice.Roll({
            event: options.event,
            parts: rollParts,
            data: data,
            skipDialog: skip,
            speaker: ChatMessage.getSpeaker({actor:this}),
            flavor: msg, 
            title: msg,
        });
    }

    rollAtaqueImprovidado(label, bonusfinal, options ={}){
        const msg = label;
        let rollParts = ["1d20", bonusfinal];
        const data = {
            actor: this.system,
            roll: {
                type: options.type,
                //target: target,
            }
        };

        let skip = options.event && options.event.ctrlKey;

        return ODDice.Roll({
            event: options.event,
            parts: rollParts,
            data: data,
            skipDialog: skip,
            speaker: ChatMessage.getSpeaker({actor:this}),
            flavor: msg, 
            title: msg,
        });
    }

    rollAprenderMagia(label, valor, options ={}){
        const msg = label;
        let rollParts = ["1d100"]; 
        const target = valor;
        
        const data = {
            actor: this.system,
            roll: {
                type: "abaixo",
                target: target,
            },
           details: (`Chance de aprender ${target}%`)
        };
        
        let skip = true;

        return ODDice.Roll({
            event: options.event,
            parts: rollParts,
            data: data,
            skipDialog: skip,
            speaker: ChatMessage.getSpeaker({actor:this}),
            flavor: msg, 
            title: msg,
        });
    }
  
    rollAtributos(atributo, options = {}){
        const label = this.system.atributos[atributo].name;
        const target = this.system.atributos[atributo].value;
        const rollParts = ["1d20"];

        const data = {
            actor: this.system,
            roll: {
                type: "check",
                target: target,
            },
            details: (`1d20 <= ${target} para sucesso`),

        };

        let skip = options.event && options.event.ctrlKey;

        //Roll and return
        return ODDice.Roll({
            event: options.event,
            parts: rollParts,
            data: data,
            skipDialog: skip,
            speaker: ChatMessage.getSpeaker({actor:this}),
            flavor: `Testando ${label}`, 
            title: `Testando ${label}`,
        });
    }

    
  
  }