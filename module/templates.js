/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
export const preloadHandlebarsTemplates = async function () {
    // Define template paths to load
    const templatePaths = [
      // Actor Sheet Partials
      'systems/olddragon/templates/partials/actor-tab-principal.html',
      'systems/olddragon/templates/partials/actor-tab-inventario.html',
      'systems/olddragon/templates/partials/actor-tab-poderes.html',
      'systems/olddragon/templates/partials/actor-tab-informacoes.html',
      'systems/olddragon/templates/partials/sidemenu.html',
      'systems/olddragon/templates/partials/main-header.html',
      'systems/olddragon/templates/partials/roll.html',
      'systems/olddragon/templates/partials/creature-tab-principal.html',
      'systems/olddragon/templates/partials/creature-header.html',
      'systems/olddragon/templates/partials/creature-tab-informacoes.html',

    ];
  
    // Load the template parts
    return loadTemplates(templatePaths);
  };