// Import Modules
import { preloadHandlebarsTemplates } from "./templates.js";
import { OldDragonActor } from "./actor/actor.js";
import { OldDragonActorSheet } from "./actor/actor-sheet.js";
import { OldDragonCreatureSheet } from "./actor/creature-sheet.js";
import { OldDragonItem } from "./item/item.js";
import { OldDragonItemSheet } from "./item/item-sheet.js";


Hooks.once('init', async function () {

  // CONFIG.OLDDRAGON = OLDDRAGON;
  game.olddragon = {
    OldDragonActor,
    OldDragonItem,
    OldDragonCreatureSheet,
    rollItemMacro
  };
  // OldDragonCreatureSheet,

  /**
   * Set an initiative formula for the system
   * @type {String}
   */
  CONFIG.Combat.initiative = {
    formula: "1d20 + @atributos.dex.value",
    decimals: 0
  };

  // Common helpers
  Handlebars.registerHelper('ifEquals', function (arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
  });
  Handlebars.registerHelper('eq', (lh, rh) => {
    return lh == rh;
  });
  Handlebars.registerHelper('gt', (lh, rh) => {
    return lh >= rh;
  });
  Handlebars.registerHelper('add', (lh, rh) => {
    return lh + rh;
  });
  Handlebars.registerHelper('isEmpty', (element) => {
    if (typeof element === undefined)
      return true;
    if (Array.isArray(element) && element.length)
      return false;
    if (element === '')
      return true;
  });

  // Define custom Entity classes
  CONFIG.Actor.documentClass = OldDragonActor;
  CONFIG.Item.documentClass = OldDragonItem;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("olddragon", OldDragonActorSheet, {
     types: ["character"],
     label: 'Personagem', 
     makeDefault: true 
    });
  Actors.registerSheet("olddragon", OldDragonCreatureSheet, { 
    types: ["creature"], 
    label: 'Criatura/Inimigo',
    makeDefault: true 
  });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("olddragon", OldDragonItemSheet, { makeDefault: true });

  // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper('concat', function () {
    var outStr = '';
    for (var arg in arguments) {
      if (typeof arguments[arg] != 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('toLowerCase', function (str) {
    return str.toLowerCase();
  });

  // Preload Handlebars Templates
  preloadHandlebarsTemplates();

});

Hooks.on('preCreateItem', (doc, createData, options, userId) => {
  var img = `systems/olddragon/assets/icons/${createData.type}.svg`;
  console.log(doc);
  console.log(createData);
  if(doc.img != 'icons/svg/item-bag.svg'){
      img = createData.img;
  }

    doc.data.update({img: img})
});

Hooks.on('createItem', (createData, options, userId) => {

});

Hooks.once("ready", async function () {
  // Wait to register hotbar drop hook on ready so that modules could register earlier if they want to
  Hooks.on("hotbarDrop", (bar, data, slot) => createOldDragonMacro(data, slot));
  return false;
});

// License and KOFI infos
Hooks.on("renderSidebarTab", async (object, html) => {
  // if (object instanceof ActorDirectory) {
  //   party.addControl(object, html);
  // }
  if (object instanceof Settings) {
    let gamesystem = html.find("#game-details");
    // License text
    const template = "systems/olddragon/templates/chat/license.html";
    const rendered = await renderTemplate(template);
    gamesystem.find(".system").append(rendered);

    // ODSRD
    let docs = html.find("button[data-action='docs']");
    const styling = "border:none;margin-right:2px;vertical-align:middle;margin-bottom:5px";
    $(`<button data-action="userguide"><img src='/systems/olddragon/assets/dragon.png' width='18' height='18' style='${styling}'/>Old Dragon SRD</button>`).insertAfter(docs);
    html.find('button[data-action="userguide"]').click(ev => {
      new FrameViewer('https://www.burobrasil.com/odsrd/', { resizable: true }).render(true);
    });
  }
});

/* -------------------------------------------- */
/*  Hotbar Macros                               */
/* -------------------------------------------- */

/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {Object} data     The dropped data
 * @param {number} slot     The hotbar slot to use
 * @returns {Promise}
 */
async function createOldDragonMacro(data, slot) {

  if (data.type !== "Item") return;
  let item = await fromUuid(data.uuid, "Item");

  //console.log(item);

  if (item === undefined) return true;

  createOldDragonItemMacro(data.id, item, slot);

  // if (!("data" in data)) return ui.notifications.warn("Você só pode criar botões de macro para itens que possui");

  return false;
}

async function createOldDragonItemMacro(id, item, slot) {

  const command = `game.olddragon.rollItemMacro("${item.name}");`;
  let macro = game.macros.find(m => (m.name === item.name) && (m.command === command));
  if (!macro) {
    macro = await Macro.create({
      name: item.name,
      type: "script",
      img: item.img,
      command: command,
      flags: { "olddragon.itemMacro": true }
    });
  }
  game.user.assignHotbarMacro(macro, slot);
  
}


/**
 * Create a Macro from an Item drop.
 * Get an existing item macro if one exists, otherwise create a new one.
 * @param {itemName}
 * @return {Promise}
 */
function rollItemMacro(itemName) {
  const speaker = ChatMessage.getSpeaker();
  let actor;
  if (speaker.token) actor = game.actors.tokens[speaker.token];
  if (!actor) actor = game.actors.get(speaker.actor);

  const item = actor ? actor.items.find(i => i.name === itemName) : null;
  if (!item) return ui.notifications.warn(`O Ator controlado não possui o item ${itemName}`);
  // Trigger the item roll
  return item.roll();
}
