export class ODDice {
  static digestResult(data, roll) {
    let result = {
      isSucess: false,
      isFailure: false,
      target: data.roll.target,
      total: roll.total,
      // details: ""
    };

    if (data.roll.type == "protecao") {
      if (result.total >= data.roll.target) {
        data.details = `<b style="color:#18520b;">Sucesso!</b>
                              <div class="rowcolor"><span class="tcat">Passou na jogada de proteção.</span></div>
                              <br/>`
      } else {
        data.details = `<b style="color:#aa0200;">Falha!</b>
                              <div class="rowcolor"><span class="tcat">Não passou na jogada de proteção...</span></div>
                              <br/>`
      }
    }

    if (data.roll.type == "moral") {
      if (result.total <= data.roll.target) {
        data.details = `<b style="color:#18520b;">Sucesso!</b>
                              <div class="rowcolor"><span class="tcat">Ânimo para lutar!</span></div>
                              <br/>`
      } else {
        data.details = `<b style="color:#aa0200;">Falha!</b>
                              <div class="rowcolor"><span class="tcat">Quer se render, recuar ou fugir a todo o custo...</span></div>
                              <br/>`
      }
    }
    
    // if (data.roll.type == "hpdv") {
    //   data.actor.data.hp.value = result.total;
    //   data.actor.data.hp.max = result.total;
    // }

    let die = roll.terms[0].total;
    if (data.roll.type == "acima" || data.roll.type == "surpresa") {
      //Jogadas de Proteção, Surpresa
      if (roll.total >= result.target) {
        result.isSuccess = true;
      } else {
        result.isFailure = true;
      }
    } else if (data.roll.type == "abaixo") {
      //Atributos
     
      if (roll.total <= result.target) {
        result.isSuccess = true;
      } else {
        result.isFailure = true;
      }
    } else if (data.roll.type == "taladino") {
      //Talentos de ladino
      if (roll.terms[0].faces == 100){
        data.details += ` (Chance = ${result.target}%)`;
      } else {
        data.details += ` (Chance <= ${result.target})`;
      }
      

      if (roll.total <= result.target) {
        result.isSuccess = true;
      } else {
        result.isFailure = true;
      }
    } else if (data.roll.type == "check") {
      // SCORE CHECKS (1s and 20s)
      if (die == 1 || (roll.total <= result.target && die < 20)) {
        result.isSuccess = true;
      } else {
        result.isFailure = true;
      }
    }

    return result;

  }

  static async sendRoll({
    parts = [],
    data = {},
    title = null,
    flavor = null,
    speaker = null,
    form = null,
  } = {}) {

    let template;
    if (data.roll?.type == "afastarmv") {
      template = "systems/olddragon/templates/chat/roll-result-repeal.html";
    } else {
      template = "systems/olddragon/templates/chat/roll-result.html";
    }
    let chatData = {
      user: game.user.id,
      speaker: speaker,
    };

    let templateData = {
      title: title,
      flavor: flavor,
      data: data,
    };

    // Optionally include a situational bonus
    if (form !== null && form.bonus?.value) {
      //console.log("Rolagem utilizado = ", data.roll);
      if (data.roll.type == "taladino") {
        data.roll.target += parseInt(form.bonus.value, 10);
        // console.log("Target = ", data.roll.target);
      } else {
        parts.push(form.bonus.value);
      }
    }

    if (form !== null && form.tipoJP?.value) {
      parts.push(form.tipoJP.value);
    }
    const roll = new Roll(parts.join("+"), data);
    (await roll.evaluate({async: true})).total;

    // Convert the roll to a chat message and return the roll
    let rollMode = game.settings.get("core", "rollMode");
    rollMode = form ? form.rollMode.value : rollMode;

    // Force blind roll (ability formulas)
    if (data.roll.blindroll) {
      rollMode = game.user.isGM ? "selfroll" : "blindroll";
    }

    if (["gmroll", "blindroll"].includes(rollMode))
      chatData["whisper"] = ChatMessage.getWhisperRecipients("GM");
    if (rollMode === "selfroll") chatData["whisper"] = [game.user.id];
    if (rollMode === "blindroll") {
      chatData["blind"] = true;
      data.roll.blindroll = true;
    }

    templateData.result = ODDice.digestResult(data, roll);

    return new Promise((resolve) => {
      roll.render().then((r) => {
        templateData.rollOD = r;
        renderTemplate(template, templateData).then((content) => {
          chatData.content = content;
          if (game.dice3d) {
            game.dice3d
              .showForRoll(
                roll,
                game.user,
                true,
                chatData.whisper,
                chatData.blind
              )
              .then((displayed) => {
                ChatMessage.create(chatData);
                resolve(roll);
              });
          } else {
            chatData.sound = CONFIG.sounds.dice;
            ChatMessage.create(chatData);
            resolve(roll);
          }
        });
      });
    });

  }

  static async Roll({
    parts = [],
    data = {},
    skipDialog = false,
    speaker = null,
    flavor = null,
    title = null,
  } = {}) {
    let rolled = false;
    let template;
    let armasEquipadas = [];

    if (data.roll.type == "protecao") {
      template = "systems/olddragon/templates/chat/roll-jp-dialog.html";
    } else if (data.roll.type == "furtivo") {
      template = "systems/olddragon/templates/chat/roll-attack-dialog.html";
      console.log(data);
      for (let item of data.actor.items) {
        if (item.type == "arma_cac" || item.type == "arma_dis") {
          armasEquipadas.push(item);
        }
      }
      if (armasEquipadas.length < 1){
        ui.notifications.error("Você precisa de uma arma equipada para usar um ataque furtivo.");
        return
      }
    } else {
      template = "systems/olddragon/templates/chat/roll-dialog.html";
    }


    let dialogData = {
      formula: parts.join(" + "),
      data: data,
      armasEquipadas: armasEquipadas,
      rollMode: game.settings.get("core", "rollMode"),
      rollModes: CONFIG.Dice.rollModes,
    };

    let rollData = {
      parts: parts,
      data: data,
      title: title,
      flavor: flavor,
      speaker: speaker,
    };

    if (skipDialog) {
      return ["ataque", "ataquecac", "ataquedist", "toquecac", "toquedist", "ataquearea", "ataquedesarmado", "furtivo"].includes(data.roll.type)
        ? ODDice.sendAttackRoll(rollData)
        : ODDice.sendRoll(rollData);
    }

    let buttons = {
      ok: {
        label: "Rolar",
        icon: '<i class="fas fa-dice-d20"></i>',
        callback: (html) => {
          rolled = true;
          rollData.form = html[0].querySelector("form");
          roll = ["ataque", "ataquecac", "ataquedist", "toquecac", "toquedist", "ataquearea", "ataquedesarmado", "furtivo"].includes(data.roll.type)
            ? ODDice.sendAttackRoll(rollData)
            : ODDice.sendRoll(rollData);
        },
      },
      cancel: {
        icon: '<i class="fas fa-times"></i>',
        label: "Cancelar",
        callback: (html) => { },
      },
    };

    const html = await renderTemplate(template, dialogData);
    let roll;

    //Create Dialog window
    return new Promise((resolve) => {
      new Dialog({
        title: title,
        content: html,
        buttons: buttons,
        default: "ok",
        close: () => {
          resolve(rolled ? roll : false);
        },
      }).render(true);
    });
  }


  static digestAttackResult(data, roll) {
    let result = {
      isSuccess: false,
      isFailure: false,
      target: "",
      total: roll.total,
      details: "",
    };

    if (data.roll.type == "toquecac" || data.roll.type == "toquedist") {
      result.details = "CD = CA base do alvo + modificador de destreza, somente.";
    }

    if (data.roll.type == "ataquedesarmado") {
            let dano = (data.actor.atributos.str.mod + 1) > 1 ? (data.actor.atributos.str.mod + 1) : 1;
      result.details = `<br/>Dano causado: <b>${dano}</b>`
    }

    //Detalhes de area aqui
    if (data.roll.type == "ataquearea") {
      result.details = `<br/>Dificuldade: ${data.roll.target}`;
      if (result.total >= data.roll.target) {
        result.details += `<br/>
                                  <b style="color:#18520b;">Sucesso!</b>`
      } else {
        result.distancia = data.roll.target - result.total;
        result.details += `<br/>
                <b style="color:#aa0200;">Errou o alvo...</b> 
                <br>
                <br>
                <div class="rowcolor">
                  <span class="tcat">Vai na direção</span> [[1d8]] <span class="tcat">, distância de</span> <b>${result.distancia}</b> m!</div>
                <div style="margin:auto; text-align: center;">
                <br>
                <span class="squarespan">8</span><span class="squarespan">1</span><span class="squarespan">2</span><br>
                <span class="squarespan">7</span><span class="squarespan" style="padding-top:0px">🎯</span><span class="squarespan">3</span><br>
                <span class="squarespan">6</span><span class="squarespan">5</span><span class="squarespan">4</span><br>
                <br>
                </div>`;
      }

    }
    
    return result;
  }

  static async sendAttackRoll({
    parts = [],
    data = {},
    title = null,
    flavor = null,
    speaker = null,
    form = null,
  } = {}) {
    const template = "systems/olddragon/templates/chat/roll-attack.html";

    let chatData = {
      user: game.user.id,
      speaker: speaker,
    };

    let templateData = {
      title: title,
      flavor: flavor,
      data: data,
      // config: CONFIG.OD,
    };

    if (form !== null && form.metros?.value) {
      data.roll.target = 10 + (form.metros.value / 3);
    }

    // Optionally include a situational bonus
    if (form !== null && form.bonus?.value) parts.push(form.bonus.value);


    if (form !== null && form.armaEquipada?.value) {
      const item = data.actor.items.find(item => item.id === form.armaEquipada.value);
      // Define the roll formula.
      let roll;
      let formula;
      let danoArma;
      const actorData = data.actor.system;
      const itemData = item.system;
      const type = item.type;

      const margemCritica = itemData.margem_critica;
      if (item.type == "arma_cac") {
        formula = `1d20+${actorData.bonuscac}+${itemData.bonus_ataque}+${itemData.bonus_magico} + 2`;
        danoArma = `(${itemData.dano}+${actorData.atributos.str.mod}+${itemData.bonus_magico}+${itemData.bonus_dano}) * ${data.roll.multiplicador}`;
      } else if (item.type == "arma_dis") {
        formula = `1d20+${actorData.bonusdist}+${itemData.bonus_ataque}+${itemData.bonus_magico} + 2`;
        danoArma = `(${itemData.dano}+${itemData.bonus_magico}+${itemData.bonus_dano}) * ${data.roll.multiplicador}`;

      }

      roll = new Roll(formula, { critico: margemCritica });
      await roll.evaluate({async: true});
      const d20RollResult = roll.dice[0].total;
      let label = `<section class="chat-message">
    <div class="chat-block">
        <div class="flexrow chat-header">
            <div class="chat-title"><h2>Ataca Furtivamente...</h2></div>
            <div class="chat-img" style="background-image:url('${data.actor.img}'); flex: 0 0 42px; background-size: cover; height: 42px;"></div>
        </div>
        <div class="chat-details">...com ${item.name}</div>
        `;


      let crit = '';
      let btn = '';
      if (d20RollResult >= margemCritica) {
        crit =
          `<span style='color:#18520b; font-weight: bolder'>Acerto Crítico!</span> `
        btn = `<p><button id="rollDamage"><i class="fas fa-dice-d20"></i> Rolar Dano Crítico Furtivo</button></p>`;
        label += crit + btn;
        danoArma += `*${itemData.dano_critico}`;
      } else if (d20RollResult > 1) {
        btn = `<p><button id="rollDamage"><i class="fas fa-dice-d20"></i> Rolar Dano Furtivo</button></p>`;
        label += btn;
      } else {
        crit = `<span style='color:#aa0200; font-weight: bolder'>Falha Crítica...</span> `
        label += crit;
      }
      label += `</div></section>`

      //    Roll and send to chat.
      roll.toMessage({
        speaker: ChatMessage.getSpeaker({ actor: actorData }),
        flavor: label,
        crit: crit,
      });

      Hooks.once('renderChatMessage', (chatItem, html) => {
        html.find("#rollDamage").click(() => {
          new Roll(danoArma).evaluate({async:false}).toMessage({
            speaker: ChatMessage.getSpeaker({ actor: data.actor }),
            flavor: "Dano causado"
          });
        });
      });

      return;

    }


    const roll = new Roll(parts.join("+"), data);
    (await roll.evaluate({async: true})).total;
    // const dmgRoll = new Roll(data.roll.dmg.join("+"), data).roll();

    // Convert the roll to a chat message and return the roll
    let rollMode = game.settings.get("core", "rollMode");
    rollMode = form ? form.rollMode.value : rollMode;

    // Force blind roll (ability formulas)
    if (data.roll.blindroll) {
      rollMode = game.user.isGM ? "selfroll" : "blindroll";
    }

    if (["gmroll", "blindroll"].includes(rollMode))
      chatData["whisper"] = ChatMessage.getWhisperRecipients("GM");
    if (rollMode === "selfroll") chatData["whisper"] = [game.user.id];
    if (rollMode === "blindroll") {
      chatData["blind"] = true;
      data.roll.blindroll = true;
    }

    templateData.result = ODDice.digestAttackResult(data, roll);

    return new Promise((resolve) => {
      roll.render().then((r) => {
        templateData.rollOD = r;
        // dmgRoll.render().then((dr) => {
        //   templateData.rollDamage = dr;
        renderTemplate(template, templateData).then((content) => {
          chatData.content = content;
          // 2 Step Dice So Nice
          if (game.dice3d) {
            game.dice3d
              .showForRoll(
                roll,
                game.user,
                true,
                chatData.whisper,
                chatData.blind
              )
              .then(() => {
                // if (templateData.result.isSuccess) {
                //   templateData.result.dmg = dmgRoll.total;
                //   game.dice3d
                //     .showForRoll(
                //       dmgRoll,
                //       game.user,
                //       true,
                //       chatData.whisper,
                //       chatData.blind
                //     )
                //     .then(() => {
                //       ChatMessage.create(chatData);
                //       resolve(roll);
                //     });
                // } else {
                ChatMessage.create(chatData);
                resolve(roll);
                // }
              });
          } else {
            chatData.sound = CONFIG.sounds.dice;
            ChatMessage.create(chatData);
            resolve(roll);
          }
        });
        // });
      });
    });
  }
}