/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class OldDragonItemSheet extends ItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["olddragon", "sheet", "item"],
      width: 560,
      height: 'auto',
      resizable: true
    });
  }

  /** @override */
  get template() {
    const path = "systems/olddragon/templates/item";
    // Return a single sheet for all item types.
    //return `${path}/item-sheet.html`;
    return `${path}/${this.item.type}-sheet.html`;
  }

  /* -------------------------------------------- */

  /** @override */
  async getData(options) {

    const data = await super.getData(options);
    data.isOwned = this.item.isOwned;
    //data.config = CONFIG.OLDDRAGON;
    const actor = this.item.actor;
    data.system = data.item.system;

    //console.log(actor);
    // const ownerIsWildcard = actor && actor.system['system'].wildcard;
    // if (ownerIsWildcard || !this.item.isOwned) {
    //     data.system.ownerIsWildcard = true;
    // }
    return data;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.profile-img').contextmenu((e) => {
      new ImagePopout(this.item.img, {
          title: this.item.name,
          shareable: true,
          entity: { type: 'Item', id: this.item.id },
      }).render(true);
    });

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Roll handlers, click handlers, etc. would go here.
  }
}
