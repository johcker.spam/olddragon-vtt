/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
import { ODDice } from "../dice.js";

export class OldDragonItem extends Item {
  /**
   * Augment the basic Item data model with additional dynamic data.
   */

  chatTemplate = {
    "arma_cac":"systems/olddragon/templates/chat/roll-attack.html",
    "arma_dis":"systems/olddragon/templates/chat/roll-attack.html"
  }
  

  prepareBaseData() {
    super.prepareBaseData();

    // Get the Item's data
    const itemData = this.system;
    const actorData = this.actor ? this.actor.system : {};
    const data = itemData;
  }


  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  async roll() {
    // Basic template rendering data
    const token = this.actor.token;
    const item = this;
    const actorData = this.actor ? this.actor.system : {};
    const itemData = item.system;
    const type = item.type;

     if(type == "magia"){
       this.show();
       return
     }

    // Define the roll formula.
    let roll;
    let parts;
    let danoArma;

    // Dialogo
    let rolled = false;
    let template = "systems/olddragon/templates/chat/roll-wattack-dialog.html";

    //inicio
    const margemCritica = itemData.margem_critica;
    //console.log(item);

    if(item.type == "arma_cac"){
      parts = ["1d20" , actorData.bonuscac , itemData.bonus_magico, itemData.bonus_ataque ]; //`1d20+${actorData.bonuscac}+${itemData.bonus_ataque}+${itemData.bonus_magico}`;
      danoArma = `(${itemData.dano}+${actorData.atributos.str.mod}+${itemData.bonus_magico}+${itemData.bonus_dano})`;    
    } else if (item.type == "arma_dis"){
      parts = ["1d20", actorData.bonusdist, itemData.bonus_magico, itemData.bonus_ataque ]; //`1d20+${actorData.bonusdist}+${itemData.bonus_ataque}+${itemData.bonus_magico}`;
      danoArma = `(${itemData.dano}+${itemData.bonus_magico}+${itemData.bonus_dano})`;    
    } else if (item.type == "ataque"){
      parts = ["1d20",  itemData.bonus_ataque]; //`1d20+${itemData.bonus_ataque}`;
      danoArma = `(${itemData.dano}+${itemData.bonus_dano})`;
    }
    //fim

    const data = {
      actor: actorData,
      roll: {
          type: item.type
      },
      danoArma : danoArma,
      margemCritica: margemCritica
    };

    let dialogData = {
      formula: parts.join(" + "),
      data: data,
      rollMode: game.settings.get("core", "rollMode"),
      rollModes: CONFIG.Dice.rollModes,
    };

    let rollData = {
      parts: parts,
      data: data,
      // title: title,
      // flavor: flavor,
      // speaker: speaker,
    };

    let buttons = {
      ok: {
        label: "Rolar",
        icon: '<i class="fas fa-dice-d20"></i>',
        callback: (html) => {
          rolled = true;
          rollData.form = html[0].querySelector("form");
          roll = this.rollAtaque(rollData);
        },
      },
      cancel: {
        icon: '<i class="fas fa-times"></i>',
        label: "Cancelar",
        callback: (html) => { },
      },
    };

    const html = await renderTemplate(template, dialogData);
    //let roll;

    //Create Dialog window
    return new Promise((resolve) => {
      new Dialog({
        title: "Ataque",
        content: html,
        buttons: buttons,
        default: "ok",
        close: () => {
          resolve(rolled ? roll : false);
        },
      }).render(true);
    });
   }

   async rollAtaque(rollData){
    const item = this;
    const itemData = item.system;
    let danoArma = rollData.data.danoArma;
    let margemCritica = rollData.data.margemCritica;
    if (rollData.form !== null && rollData.form.bonus?.value) {
        rollData.parts.push(rollData.form.bonus.value);
      
    }

    let roll = (await new Roll(rollData.parts.join ("+")).evaluate({async:false}));
    const d20RollResult = roll.dice[0].total;
    let label = `<section class="chat-message">
    <div class="chat-block">
        <div class="flexrow chat-header">
            <div class="chat-title"><h2>Ataca com ${item.name}</h2></div>
            <div class="chat-img" style="background-image:url('${this.actor.img}'); flex: 0 0 42px; background-size: cover; height: 42px;"></div>
        </div>
        `;
    if (item.type == "ataque"){
      label += `<div>
      <span>Quantidade de ataques = </span><b>${itemData.qtd_ataques}</b>
      </div>`;
    }
    
    let crit = '';
    let btn = '';
    if (d20RollResult >= margemCritica){
      crit =
        `<span style='color:#18520b; font-weight: bolder'>Acerto Crítico!</span> `
      btn = `<p><button id="rollDamage"><i class="fas fa-dice-d20"></i> Rolar Dano Crítico</button></p>`;
        label += crit + btn;
      danoArma += `*${itemData.dano_critico}`;
    } else if (d20RollResult > 1){
        btn = `<p><button id="rollDamage"><i class="fas fa-dice-d20"></i> Rolar Dano</button></p>`;
        label += btn;
    } else {
      crit = `<span style='color:#aa0200; font-weight: bolder'>Falha Crítica...</span> `
        label += crit;
    }
    label += `</div></section>`

//    Roll and send to chat.
    roll.toMessage({
      speaker: ChatMessage.getSpeaker({ actor: this.actor }),
      flavor: label,
      crit: crit,
    });

    Hooks.once('renderChatMessage', (chatItem, html) => {
      html.find("#rollDamage").click(() => {
        new Roll(danoArma).evaluate({async:false}).toMessage({
          speaker: ChatMessage.getSpeaker({ actor: this.actor }),
          flavor: "Dano causado"
        });
      });
    });
   }

   async show(){
    const item = this;
    const itemData = item.system;
    const type = item.type;
    let chatTemplate = "";
    
    if(type == "magia"){
      chatTemplate = `<section class="chat-message">
      <div class="chat-block">
          <div class="flexrow chat-header">
              <div class="chat-title"><h2>${item.name}</h2></div>
              <div class="chat-img" style="background-image:url('${this.actor.img}'); flex: 0 0 42px; background-size: cover; height: 42px;"></div>
              </div><table class="repealTable">
      <thead>
          <tr>
              <th class="template-label" colspan="2" style="width:100%"> Tipo: ${itemData.tipo} | Círculo: ${itemData.circulo}</th>
          </tr>
      </thead>
      <tbody>
        <tr>
            <td class="template-label">Alcance: </td>
            <td style="width:76%">${itemData.alcance}</td>
        </tr>
        <tr>
            <td class="template-label">Duração: </td>
            <td >${itemData.duracao}</td>
        </tr>
        <tr>
            <td class="template-label">JP: </td>
            <td >${itemData.jp}</td>
        </tr>
        <tr>
            <td class="template-label">Efeito JP: </td>
            <td >${itemData.efeito_jp}</td>
        </tr>
        <tr>
            <td class="template-label">RM: </td>
            <td >${itemData.rm}</td>
        </tr>
        <tr>
            <td colspan="2">${itemData.descricao}</td>
        </tr>
        </tbody>
      </table>
      </div>
      </section>`;
    }

    // Informações básicas do chat
    const chatData = {
      user: game.user.id,
      type: CONST.CHAT_MESSAGE_TYPES.OTHER,
      content: chatTemplate,
      speaker: {
        alias: this.actor.name
      },
    };

    // Muda tipo de rolagem
    let rollMode = game.settings.get("core", "rollMode");
    if (["gmroll", "blindroll"].includes(rollMode)){
      chatData["whisper"] = ChatMessage.getWhisperRecipients("GM");
    }
    if (rollMode === "selfroll") chatData["whisper"] = [game.user.id];
    if (rollMode === "blindroll") chatData["blind"] = true;

    // Cria mensagem do chat
    return ChatMessage.create(chatData);

   }

}
