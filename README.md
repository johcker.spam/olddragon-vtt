#Old Dragon

Esta é a versão do sistema Old Dragon para o Foundry VTT. É baseada nas regras do Old Dragon Aprimorado e contém informações da SRD.
Como usuário você é responsável por incluir informações de materiais que possua de outros suplementos oficiais.

##Changelog 

Esta versão conta com os seguintes recursos:

- Ficha de personagem para PCs e NPCs, de acordo com as regras do livro básico
- Ficha de criaturas, seguindo o modelo do suplemento Bestiário
- Sub-fichas para vários tipos de itens (no contexto do Foundry: item, magia, armas, ataque, armadura, escudo, anel, pergaminho e poção)
- Cálculo automático de quase todos os recursos ou atributos secundários (como modificadores de atributos, idiomas conhecidos, número máximo de seguidores, talentos de ladrão, afastar mortos-vivos, cálculo de carga, magias conhecidas por círculo, dentre outros)
- Rolagens automáticas com mensagens de chat customizadas para ambas as fichas (destaque para os ataques, ping de magia, ataque de área, combate improvisado, afastar mortos-vivos, talentos de ladrão, jogadas de proteção, surpresa e moral)
- Recursos de arrastar e soltar para os ataques das criaturas e armas equipadas dos personagens, assim como para as magias de ambos
- Compêndios de itens (SDR)
- Compêndio de armas (SDR)
- Compêndio de armaduras e escudos (SDR)
- Compêndio de magias (SDR)
- Compêndio de monstros (SDR)
- Melhoria nas mensagens do chat

##Backlog

Funcionalidades previstas para as próximas atualizações:

- Melhoria nos diálogos das rolagens
- Aplicação dos modificadores dos níveis de carga ao cálculo de movimentação
- Tabelas de itens mágicos

##Bugs Conhecidos

- Warnings da rolagem assíncrona, sem impacto até o foundry 0.10.x

##Licença e Acesso ao SRD

O material do compêndio é regido pelos termos da licença [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR) e o SRD pode ser acessado através [deste link](https://www.burobrasil.com/odsrd/) ou através de um botão incluído na interface do Foundry VTT, na área de configurações, sub-menu de Ajuda e Documentação.

Os ícones padrão dos itens foram providos sob os termoa da licença [Creative Commons 3.0 BY](http://creativecommons.org/licenses/by/3.0/).
Os ícones foram feitos por Lorc, Delapouite e Willdabeast. E estão disponíveis em https://game-icons.net.
